#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <omp.h>

enum Color{
    EMPTY,
    RED,
    BLUE
};

size_t best_price = 0;
size_t SEQUENTIAL_THRESHOLD = 5;
size_t GENERATING = false;
size_t DEPTH_LIMIT = 7;


class Solution{
public:
    Solution()=default;
    Solution(std::vector<Color> & vertices, std::vector<std::pair<int, std::pair<size_t, size_t>>> & used_edges):
            vertices(vertices), used_edges(used_edges){};

    void display_coloring(){
        std::cout << "Red vertices:";
        for (size_t i = 0; i < vertices.size(); ++i){
            Color color = vertices[i];
            if(color == Color::RED){
                std::cout << " " << i;
            }
        }
        std::cout << std::endl << "Blue vertices:";
        for (size_t i = 0; i < vertices.size(); ++i){
            Color color = vertices[i];
            if(color == Color::BLUE){
                std::cout << " " << i;
            }
        }
        std::cout << std::endl << "Edges:" << std::endl <<"Red\t\tBlue";
        for(auto edge:used_edges){
            std::cout << std::endl;
            Color c1 = vertices[edge.second.first];
            if(c1 == Color::RED) {
                std::cout << edge.second.first << "\t<>\t" << edge.second.second;
            }
            else{
                std::cout << edge.second.second << "\t<>\t" << edge.second.first;
            }
        }
        std::cout << std::endl;
    }
private:
    std::vector<Color> vertices;
    std::vector<std::pair<int, std::pair<size_t, size_t>>> used_edges;
};

Solution best_solution;

class Graph{
public:
    Graph(std::vector<Color> & vertices_, std::vector<std::pair<int, std::pair<size_t, size_t>>> & edges_, std::vector<std::pair<int, std::pair<size_t, size_t>>> & used_edges_, size_t depth) {
        // deep copy of each vector
        vertices = std::vector<Color>(vertices_);
        edges = std::vector<std::pair<int, std::pair<size_t, size_t>>>(edges_);
        used_edges = std::vector<std::pair<int, std::pair<size_t, size_t>>>(used_edges_);
        this->depth = depth;
    }

    void explore(std::vector<Graph> & init_states) {
        // DATA increase currently expanding depth
        // when returning from successful expansion decrease the depth
        if(! is_bipartite()){
            return;
        }
        // BnB with connectivity limit
        // connected graph must have |E| >= |V|-1
        if ((used_edges.size() + edges.size()) < (vertices.size() - 1)){
            return;
        }
        size_t current_price = 0;
        for(auto edge: used_edges){
            current_price += edge.first;
        }
        if (edges.empty()){
            if(current_price > best_price && is_connected(used_edges)){
#pragma omp critical
                if(current_price > best_price && is_connected(used_edges)) {
                    best_price = current_price;
                    best_solution = Solution(vertices, used_edges);
                }
            }
            return;
        }
        size_t possible_gain = 0;
        for(auto edge: edges){
            possible_gain += edge.first;
        }
        // BnB with upperbound price of the subgraph
        if(current_price + possible_gain <= best_price){
            return;
        }
        if(current_price > best_price && is_connected(used_edges)) {
#pragma omp critical
            if(current_price > best_price && is_connected(used_edges)) {
                best_price = current_price;
                best_solution = Solution(vertices, used_edges);
            }
        }
        this->depth += 1;
        // DATA generating data up to certain depth
        if(GENERATING && (this->depth >= DEPTH_LIMIT)) {
            init_states.emplace_back(vertices, edges, used_edges, this->depth);
            this->depth -= 1;
            return;
        }
        // tentatively select the most expensive edge
        auto edge_i = edges.size() - 1;
        size_t v1, v2;
        for(int i = edges.size()-1; i >=0; --i){
            auto e = edges[i];
            v1 = e.second.first, v2 = e.second.second;
            if(vertices[v1] != Color::EMPTY || vertices[v2] != Color::EMPTY){
                // find edge which has at least one color unassigned
                edge_i = i;
                break;
            }
        }
        auto top_edge = edges[edge_i];
        v1 = top_edge.second.first, v2 = top_edge.second.second;
        Color c1 = vertices[v1], c2 = vertices[v2];
        edges.erase(edges.begin()+edge_i);
        // edges are sorted in ascending order
        used_edges.push_back(top_edge);

        // the edge can be trivially included because both ends have different color
        if(vertices[v1] != vertices[v2] && vertices[v1] != Color::EMPTY && vertices[v2] != Color::EMPTY){
            Graph graph(vertices, edges, used_edges, this->depth);
            graph.explore(init_states);
            // DATA when returning from successful expansion decrease the depth
        }
        if(vertices[v1] == Color::EMPTY && vertices[v2] != Color::EMPTY){
            // only one way to color this edge
            vertices[v1] = vertices[v2] == Color::RED ? Color::BLUE : Color::RED;
            Graph graph(vertices, edges, used_edges, this->depth);
            graph.explore(init_states);
        }
        else if(vertices[v2] == Color::EMPTY && vertices[v1] != Color::EMPTY){
            // only one way to color this edge
            vertices[v2] = vertices[v1] == Color::RED ? Color::BLUE : Color::RED;
            Graph graph(vertices, edges, used_edges, this->depth);
            graph.explore(init_states);
        }
        else if(vertices[v1] == Color::EMPTY && vertices[v2] == Color::EMPTY){
            // both edges are uncolored, try both combinations
            // color edge Red
            vertices[v1] = Color::RED;
            // opposite color
            vertices[v2] = Color::BLUE;
            // continue recursion with another edge
            Graph graph(vertices, edges, used_edges, this->depth);
            graph.explore(init_states);
            // color edge Blue
            vertices[v1] = Color::BLUE;
            // opposite color
            vertices[v2] = Color::RED;
            graph = Graph(vertices, edges, used_edges, this->depth);
            graph.explore(init_states);
        }

        // do not include this edge, remove it and continue with recursion
        // this edge was not included so if any vertex is colored the other can be colored the same
        used_edges.pop_back();
        if(c1 == Color::EMPTY) {
            vertices[v1] = c2;
            vertices[v2] = c2;
        }else if(c2 == Color::EMPTY) {
            vertices[v1] = c1;
            vertices[v2] = c1;
        }

        explore(init_states);
    }

    bool is_connected(std::vector<std::pair<int, std::pair<size_t, size_t>>> & edges_){
        // test of components, if each vertex has same number there is only one component and graph is connected
        std::vector<std::pair<int, std::pair<size_t, size_t>>> connectivity(edges_);
        auto top_edge = connectivity.front();
        size_t current_v = top_edge.second.first;
        size_t top_v = current_v;
        // propagate number top_v to whole component
        propagate(current_v, top_v, connectivity);
        for(auto edge: connectivity){
            auto v1 = edge.second.first, v2 = edge.second.second;
            if (v1 != top_v || v2 != top_v) return false;
        }
        return true;
    }

    void propagate(size_t current_v, size_t top_v, std::vector<std::pair<int, std::pair<size_t, size_t>>> & connectivity){
        for(size_t i = 0; i < connectivity.size(); ++i){
            auto v1 = connectivity[i].second.first, v2 = connectivity[i].second.second;
            if(v1 == current_v && v2 != top_v){
                connectivity[i].second.first = top_v;
                connectivity[i].second.second = top_v;
                propagate(v2, top_v, connectivity);
            } else if(v2 == current_v && v1 != top_v){
                connectivity[i].second.first = top_v;
                connectivity[i].second.second = top_v;
                propagate(v1, top_v, connectivity);
            }
        }
    }

    bool is_bipartite() {
        // tests if all connected vertices have different colors
        for(auto e: used_edges) {
            auto v1 = e.second.first;
            auto v2 = e.second.second;
            // conflict of the same color
            if(vertices[v1] == vertices[v2] && vertices[v1] != Color::EMPTY){
                return false;
            }
        }
        return true;
    }

    std::vector<Color> & get_vertices(){
        return vertices;
    }

    std::vector<std::pair<int, std::pair<size_t, size_t>>> & get_edges(){
        return edges;
    }

    std::vector<std::pair<int, std::pair<size_t, size_t>>> get_used_edges(){
        return used_edges;
    }

    size_t value()const{
        size_t current_price = 0;
        for(auto edge: used_edges){
            current_price += edge.first;
        }
        size_t possible_gain = 0;
        for(auto edge: edges){
            possible_gain += edge.first;
        }
        return current_price;
    }

    bool operator <(const Graph & other)const{
        return this->value() < other.value();
    }

private:
    std::vector<Color> vertices;
    std::vector<std::pair<int, std::pair<size_t, size_t>>> edges;
    std::vector<std::pair<int, std::pair<size_t, size_t>>> used_edges;
    size_t depth=0;
};

Graph load(size_t N, size_t K, const std::string& filename){
    std::ifstream infile(filename);
    size_t n;
    // upper bound
    size_t ub = 0;
    infile >> n;
    // price of the edge in 2D adjacency matrix
    std::vector<std::vector<int>> adj_matrix(n, std::vector<int>(n, 0));
    // price, <v1, v2>
    // edges do not repeat
    std::vector<std::pair<int, std::pair<size_t, size_t>>> edges;
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            // price is non-negative
            int price;
            infile >> price;
            adj_matrix[i][j] = price;
            // do not repeat the same edge
            if (adj_matrix[i][j] != adj_matrix[j][i]) {
                edges.emplace_back(price, std::pair(i, j));
                ub += price;
            }
        }
    }
    std::vector<Color> vertices(n, Color::EMPTY);
    std::vector<std::pair<int, std::pair<size_t, size_t>>> used_edges;

    // increasing order
    std::sort(edges.begin(), edges.end(), [](auto &left, auto &right)
    {return left.first < right.first;});
    Graph graph(vertices, edges, used_edges, 0);
    if(! graph.is_connected(edges)){
        std::cout << filename << " graph is not connected." << std::endl;
    }
    return graph;
}


void run(Graph & graph, bool display=false){
    // container for states
    std::vector<Graph> init_states;
    // graph expands up to depth==DEPTH_LIMIT and pushes the state to init_states
    GENERATING = false;
    auto start = std::chrono::high_resolution_clock::now();
    graph.explore(init_states);
    auto stop = std::chrono::high_resolution_clock::now();
    if(display) best_solution.display_coloring();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Total price:\t\t" << best_price << std::endl
              << "Time in seconds:\t"<< double(duration.count()) / 1000 << std::endl;
    size_t n_limit = init_states.size();
    std::sort(init_states.begin(), init_states.end());
    std::cout << "N of init states: " << n_limit << std::endl;
    return;
    // do not generate anything in next run
    GENERATING = false;
    // not important, just because method signature requires it
    std::vector<Graph> empty;
    for(size_t i=0; i < n_limit; i++){
        auto state =  init_states[i];
        state.explore(empty);
    }
//    auto start = std::chrono::high_resolution_clock::now();
//    auto stop = std::chrono::high_resolution_clock::now();
//    if(display) best_solution.display_coloring();
//    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
//    std::cout << "Total price:\t\t" << best_price << std::endl
//              << "Time in seconds:\t"<< double(duration.count()) / 1000 << std::endl;
}


void testing(std::string filename){
    std::ifstream infile(filename);
    std::string graph_file;
    double ref_time;
    size_t ref_num;
    size_t ref_price;
    size_t passed_ctr = 0;
    size_t total_ctr = 0;
    double seq_ref_time = 0;
    while (true) {
        infile >> graph_file >> std::skipws >> ref_time >> seq_ref_time >> ref_num >> ref_price;
        if( infile.eof() ) break;
        ++total_ctr;
        // reset global counter
        best_price = 0;
        best_solution = Solution();
        graph_file.insert(0, "graf_bpo/");
        Graph graph = load(0, 0, graph_file);
        std::cout << graph_file << std::endl;
        run(graph, false);
        std::string result = best_price == ref_price ? "PASSED" : "FAILED";
        if (best_price == ref_price) ++passed_ctr;
        std::cout << "REFERENCE:" << std::endl;
        std::cout << "Total price:\t\t" << ref_price << std::endl << "# of recursions:\t" << ref_num << "K" << std::endl << "Time in seconds:\t"<< ref_time << " (" << seq_ref_time << ") - sequential" << std::endl;
        std::cout << "====================" << result << "====================" << std::endl;
    }
    std::cout << passed_ctr << "/" << total_ctr << " passed." << std::endl;
}



int main(int argc, char *argv[]) {
    if(argc == 2){
        std::string filename(argv[1]);
        std::cout << "====================" << "TESTING" << "====================" << std::endl;
        testing(filename);
        return 0;
    }
    size_t N = std::strtol(argv[1], nullptr, 10);
    size_t K = std::strtol(argv[2], nullptr, 10);
    std::string filename(argv[3]);
    Graph graph = load(N, K, filename);
    run(graph, true);
}