#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>
#include <omp.h>
#include <numeric>
#include <mpi.h>
#include <unistd.h>

enum Color{
    EMPTY,
    RED,
    BLUE
};

size_t best_price = 0;
size_t master_price = 0;
bool GENERATE = false;
// results in 2^DEPTH_LIMIT initial solutions
size_t DEPTH_LIMIT = 8;
size_t SLAVE_DEPTH_LIMIT = 6;
int rank = 0;

int TAG_WORK = 0;
int TAG_DONE = 1;
int TAG_TERMINATE = 2;
int DEST_MASTER = 0;
MPI_Datatype solution_type;

typedef struct StructSolution{
    StructSolution(){};
    StructSolution(const std::vector<Color> & vertices,
                const std::vector<std::pair<int, std::pair<size_t, size_t>>> & edges,
                const std::vector<std::pair<int, std::pair<size_t, size_t>>> & used_edges){
        for(int i = 0; i < vertices.size(); ++i){
            this->vertices[i] = vertices[i];
        }
        this->n_vertices = vertices.size();
        size_t  n_unused_edges = edges.size(),
                n_used_edges = used_edges.size();
            
        this->n_edges = n_unused_edges + n_used_edges;
        for(size_t i=0; i < n_unused_edges; ++i){
            prices[i] = edges[i].first;
            edge_start[i] = edges[i].second.first;
            edge_end[i] = edges[i].second.second;
            edge_included[i] = false;
        }
        for(size_t i=0; i < n_used_edges; ++i){
            prices[i+n_unused_edges] = used_edges[i].first;
            edge_start[i+n_unused_edges] = used_edges[i].second.first;
            edge_end[i+n_unused_edges] = used_edges[i].second.second;
            edge_included[i+n_unused_edges] = true;
        }
    }

    void display_coloring(){
        std::cout << "Red vertices:";
        for (size_t i = 0; i < n_vertices; ++i){
            Color color = vertices[i];
            if(color == Color::RED){
                std::cout << " " << i;
            }
        }
        std::cout << std::endl << "Blue vertices:";
        for (size_t i = 0; i < n_vertices; ++i){
            Color color = vertices[i];
            if(color == Color::BLUE){
                std::cout << " " << i;
            }
        }
        std::cout << std::endl;
        for (size_t i = 0; i < n_vertices; ++i){
            std::cout << vertices[i];
        }

        std::cout << std::endl;
        for (size_t i = 0; i < n_edges; ++i){
            std::cout << edge_included[i];
        }
        std::cout << std::endl << n_vertices << " " << n_edges;
        std::cout << std::endl << "Edges: " << std::endl <<"Red\t\t Blue";
        for(int i = 0; i < n_edges; ++i){
            if(edge_included[i]){
                size_t  e_start = edge_start[i],
                        e_end = edge_end[i];
                Color   c1 = vertices[e_start];
                if(c1 == Color::RED) {
                    std::cout << std::endl;
                    std::cout << e_start << "\t<>\t" << e_end;
                }
                if(c1 == Color::BLUE) {
                    std::cout << std::endl;
                    std::cout << e_end << "\t<>\t" << e_start;
                }
            }
        }
        std::cout << "\nRemaining edges:";
        for(int i = 0; i < n_edges; ++i){
            if(! edge_included[i]){
                size_t e_start = edge_start[i];
                size_t e_end = edge_end[i];
                std::cout << " (" << e_start << "," << e_end << ")";
            }
        }
        std::cout << std::endl;
    }

    int get_price() const{
        int price = 0;
        for(int i = 0; i < n_edges; ++i)
            if(edge_included[i])
                price += prices[i];
        return price;
    }

    int n_vertices=0;
    int n_edges=0;
    Color vertices[1000]{};
    int prices[1000]{};
    int edge_start[1000]{};
    int edge_end[1000]{};
    bool edge_included[1000]{};
} StructSolution;

StructSolution best_solution;
StructSolution master_solution;

class Graph;
std::vector<Graph> states;
class Graph{
public:
    // used to make duplicate of state 
    Graph(const std::vector<Color>& vertices_,
        const std::vector<std::pair<int, std::pair<size_t, size_t>>>& edges_,
        const std::vector<std::pair<int, std::pair<size_t, size_t>>>& used_edges_,
        const size_t depth=0)
            : vertices{vertices_}, edges{edges_}, used_edges{used_edges_}, depth{depth} {}
    // used after MPI_Recv in slave
    explicit Graph(const StructSolution & s_solution){
        vertices.resize(s_solution.n_vertices);
        for(int i = 0; i < s_solution.n_vertices; ++i){
            vertices[i] = s_solution.vertices[i];
        }
        size_t n_edges = s_solution.n_edges;
        size_t n_unused_edges = 0;
        size_t n_used_edges = 0;
        for(size_t i = 0; i < n_edges; ++i){
            if(s_solution.edge_included[i])
                ++n_used_edges;
            else
                ++n_unused_edges;
        }
        edges.resize(n_unused_edges);
        used_edges.resize(n_used_edges);
        size_t used_ctr = 0, unused_ctr = 0;
        for(size_t i=0; i < n_edges; ++i){
            int price = s_solution.prices[i];
            size_t edge_start = s_solution.edge_start[i];
            size_t edge_end = s_solution.edge_end[i];
            if(s_solution.edge_included[i])
                used_edges[used_ctr++] = std::pair(price, std::pair(edge_start, edge_end));
            else
                edges[unused_ctr++] = std::pair(price, std::pair(edge_start, edge_end));
        }
        depth=0;
        }

    void print_state(){
        for(int i = 0; i < vertices.size(); i++){
            std::cout << vertices[i];
        }
        std::cout << "\t" << rank << std::endl;
    }

    void explore(){
        if(! is_bipartite()){
            return;
        }
        // BnB with connectivity limit
        // connected graph must have |E| >= |V|-1
        if ((used_edges.size() + edges.size()) < (vertices.size() - 1)){
            return;
        }
        size_t current_price = 0;
        for(auto edge: used_edges){
            current_price += edge.first;
        }
        if (edges.empty()){
            if(current_price > best_price && is_connected(used_edges)){
#pragma omp critical
                if(current_price > best_price && is_connected(used_edges)) {
                    best_price = current_price;
                    best_solution = StructSolution(vertices, edges, used_edges);
                }
            }
            return;
        }
        size_t possible_gain = 0;
        for(auto edge: edges){
            possible_gain += edge.first;
        }
        // BnB with upperbound price of the subgraph
        if(current_price + possible_gain <= best_price){
            return;
        }
        if(current_price > best_price && is_connected(used_edges)) {
#pragma omp critical
            if(current_price > best_price && is_connected(used_edges)) {
                best_price = current_price;
                best_solution = StructSolution(vertices, edges, used_edges);
            }
        }
        // DATA increase currently expanding depth
        // when returning from successful expansion decrease the depth
        this->depth += 1;
        if(GENERATE){
            if (this->depth >= DEPTH_LIMIT){
//                this->print_state();
                states.emplace_back(vertices, edges, used_edges, depth);
                return;
            }
        }
        auto edge_i = edges.size() - 1;
        size_t v1, v2;
        for(int i = edges.size()-1; i >=0; --i){
            auto e = edges[i];
            v1 = e.second.first, v2 = e.second.second;
            if(vertices[v1] != Color::EMPTY || vertices[v2] != Color::EMPTY){
                edge_i = i;
                break;
            }
        }
        auto top_edge = edges[edge_i];
        v1 = top_edge.second.first, v2 = top_edge.second.second;
        Color c1 = vertices[v1], c2 = vertices[v2];

        edges.erase(edges.begin()+edge_i);
        // edges are sorted in ascending order
        used_edges.push_back(top_edge);
        // the edge can be trivially included because both ends are have color
        // do not explore without it
        if(vertices[v1] != vertices[v2] && vertices[v1] != Color::EMPTY && vertices[v2] != Color::EMPTY){
            explore();
            return;
        }
        if(vertices[v1] == Color::EMPTY && vertices[v2] != Color::EMPTY){
            vertices[v1] = vertices[v2] == Color::RED ? Color::BLUE : Color::RED;
            Graph graph(vertices, edges, used_edges, this->depth);
            graph.explore();
        }
        else if(vertices[v2] == Color::EMPTY && vertices[v1] != Color::EMPTY){
            vertices[v2] = vertices[v1] == Color::RED ? Color::BLUE : Color::RED;
            Graph graph(vertices, edges, used_edges, this->depth);
            graph.explore();
        }
        else if(vertices[v1] == Color::EMPTY && vertices[v2] == Color::EMPTY){
            // color edge Red
            vertices[v1] = Color::RED;
            // opposite color
            vertices[v2] = Color::BLUE;
            // continue recursion with another edge
            Graph graph(vertices, edges, used_edges, this->depth);
            graph.explore();
            // color edge Blue
            vertices[v1] = Color::BLUE;
            // opposite color
            vertices[v2] = Color::RED;
            Graph graph2 = Graph(vertices, edges, used_edges, this->depth);
            graph2.explore();
        }
        // do not include this edge, remove it and continue with recursion
        // this edge was not included so if any vertex is colored the other can be colored the same
        used_edges.pop_back();
        if(c1 == Color::EMPTY) {
            vertices[v1] = c2;
            vertices[v2] = c2;
        }else if(c2 == Color::EMPTY) {
            vertices[v1] = c1;
            vertices[v2] = c1;
        }
        explore();
    }

    void coloring(){
        for(auto c: vertices){
            std::cout << c;
        }
        for(auto c: edges){
            std::cout << "(" << c.second.first << "," << c.second.second << ") ";
        }
        std::cout << std::endl;

        for(auto c: used_edges){
            std::cout << "(" << c.second.first << "," << c.second.second << ") ";
        }
        std::cout << std::endl;
    }

    bool is_connected(const std::vector<std::pair<int, std::pair<size_t, size_t>>> & edges_){
        // test of components, if each vertex has same number there is only one component and graph is connected
        std::vector<std::pair<int, std::pair<size_t, size_t>>> connectivity(edges_);
        auto top_edge = connectivity.front();
        size_t current_v = top_edge.second.first;
        size_t top_v = current_v;
        // propagate number top_v to whole component
        propagate(current_v, top_v, connectivity);
        for(auto edge: connectivity){
            auto v1 = edge.second.first, v2 = edge.second.second;
            if (v1 != top_v || v2 != top_v) return false;
        }
        return true;
    }

    void propagate(size_t current_v, size_t top_v, std::vector<std::pair<int, std::pair<size_t, size_t>>> & connectivity){
        for(size_t i = 0; i < connectivity.size(); ++i){
            auto v1 = connectivity[i].second.first, v2 = connectivity[i].second.second;
            if(v1 == current_v && v2 != top_v){
                connectivity[i].second.first = top_v;
                connectivity[i].second.second = top_v;
                propagate(v2, top_v, connectivity);
            } else if(v2 == current_v && v1 != top_v){
                connectivity[i].second.first = top_v;
                connectivity[i].second.second = top_v;
                propagate(v1, top_v, connectivity);
            }
        }
    }

    bool is_bipartite() {
        // tests if all connected vertices have different colors
        for(auto e: used_edges) {
            auto v1 = e.second.first;
            auto v2 = e.second.second;
            // conflict of the same color
            if(vertices[v1] == vertices[v2] && vertices[v1] != Color::EMPTY){
                return false;
            }
        }
        return true;
    }

    size_t value()const{
        size_t current_price = 0;
        for(auto edge: used_edges){
            current_price += edge.first;
        }
        // size_t possible_gain = 0;
        // for(auto edge: edges){
        //     possible_gain += edge.first;
        // }
        return current_price;
    }

    bool operator <(const Graph & other)const{
        return this->value() < other.value();
    }

    bool operator >(const Graph & other)const{
        return this->value() > other.value();
    }

    StructSolution serialize() const{
        StructSolution s(vertices, edges, used_edges);
        return s;
    }

private:
    std::vector<Color> vertices;
    std::vector<std::pair<int, std::pair<size_t, size_t>>> edges;
    std::vector<std::pair<int, std::pair<size_t, size_t>>> used_edges;
    size_t depth=0;
};

Graph load(size_t N, size_t K, const std::string& filename){
    std::ifstream infile(filename);
    size_t n;
    // upper bound
    size_t ub = 0;
    infile >> n;
    // price of the edge in 2D adjacency matrix
    std::vector<std::vector<int>> adj_matrix(n, std::vector<int>(n, 0));
    // price, <v1, v2>
    // edges do not repeat
    std::vector<std::pair<int, std::pair<size_t, size_t>>> edges;
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            // price is non-negative
            int price;
            infile >> price;
            adj_matrix[i][j] = price;
            // do not repeat the same edge
            if (adj_matrix[i][j] != adj_matrix[j][i]) {
                edges.emplace_back(price, std::pair(i, j));
                ub += price;
            }
        }
    }
    std::vector<Color> vertices(n, Color::EMPTY);
    std::vector<std::pair<int, std::pair<size_t, size_t>>> used_edges;

    // increasing order
    std::sort(edges.begin(), edges.end(), [](auto &left, auto &right)
    {return left.first < right.first;});
    Graph graph(vertices, edges, used_edges);
    if(! graph.is_connected(edges)){
        std::cout << filename << " graph is not connected." << std::endl;
    }
    return graph;
}

void update(const StructSolution & s){
    int new_price = s.get_price();
    if(new_price > master_price){
        std::cout << "\t\t\t" << master_price << "/" << new_price << " INCREASE" << std::endl;
        std::cout << omp_get_thread_num() << " ends" << " " << new_price << std::endl;
        master_price = new_price;
        master_solution = s;
    }
}

void runSlave(Graph & graph){
    // DATA container for states
    states.clear();
    GENERATE = true;
    // graph expands up to depth==DEPTH_LIMIT and pushes the state to init_states
    graph.explore();
    GENERATE = false;
//    std::cout << rank << " precomputed " << states.size() << " " << best_price << std::endl;
//     DATA - sort states according to price found in the state
    std::sort(states.begin(), states.end());
    int n = states.size();
#pragma omp parallel for
       for(int i = 0; i < n; ++i){
        std::cout << rank << "|thread:" << omp_get_thread_num() << " starts" << std::endl;
        states[i].explore();
        std::cout << rank << "|thread:" << omp_get_thread_num() << " ENDS " << i << std::endl;
    }
}

void run(Graph & graph, bool display=false){
    // DATA container for states
    states.clear();
    GENERATE = true;
    // graph expands up to depth==DEPTH_LIMIT and pushes the state to init_states
    // reset global counter
    best_price = 0;
    auto start = std::chrono::steady_clock::now();
    graph.explore();
    GENERATE = false;
    std::cout << rank << " N. of initial states: " << states.size() << std::endl;
    // DATA - sort states according to price found in the state
    states.pop_back();
    std::sort(states.begin(), states.end());
    // DATA container needed because of signature
    MPI_Status status;
    int p;
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    int working_slaves = p - 1;
    int dest;
    // master distributes initial work to all slaves
    std::cout << working_slaves << " slaves available" << std::endl;
    for (int i=1; i < p; ++i) {
        size_t n_states = states.size();
        dest = i;
        if (n_states > 0) {
            StructSolution serialized_solution;
            // Graph to serialized_solution
            serialized_solution = states.back().serialize();
            states.pop_back();
            MPI_Ssend(&serialized_solution,
                    1,
                    solution_type, dest, TAG_WORK, MPI_COMM_WORLD);
        }
        // there is less work than slaves, free remaining slaves
        else {
            StructSolution done_solution;
            MPI_Ssend(&done_solution, sizeof(StructSolution), MPI_BYTE, dest, TAG_TERMINATE, MPI_COMM_WORLD);
            working_slaves--;
        }
    }
    // master waits for answers and gives more work
    std::cout << "WAITING for \t\t" << working_slaves << " slaves available" << std::endl;
    int flag;
    while(working_slaves > 0){
        MPI_Iprobe(MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &flag, &status);
        // active waiting
        if(flag && status.MPI_TAG == TAG_DONE) {
            StructSolution done_solution;

            MPI_Recv(&done_solution,
                    1,
                    solution_type, MPI_ANY_SOURCE, TAG_DONE, MPI_COMM_WORLD, &status);
            // if found solution is better than before update local value
            update(done_solution);

            size_t n_states = states.size();
            // more work to be done
            if (n_states > 0) {
                StructSolution serialized_solution;
                serialized_solution = states.back().serialize();
                states.pop_back();
                MPI_Ssend(&serialized_solution,
                        1,
                        solution_type, status.MPI_SOURCE, TAG_WORK, MPI_COMM_WORLD);
            }
                // stop remaining slaves, no more work
            else {
                StructSolution serialized_solution;
                std::cout << "Master stopping " << status.MPI_SOURCE << std::endl;
                MPI_Ssend(&serialized_solution,
                        1,
                        solution_type, status.MPI_SOURCE, TAG_TERMINATE, MPI_COMM_WORLD);
                working_slaves--;
                std::cout << working_slaves << " slaves available" << std::endl;
            }
            std::cout << "WAITING for \t\t" << working_slaves << " slaves available" << std::endl;
        }
    }
    std::cout << "Master FINISH" << std::endl;
    auto stop = std::chrono::steady_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);

    if(display) master_solution.display_coloring();

    duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Total price:\t\t" << master_price << std::endl << "Time in seconds:\t"<< double(duration.count()) / 1000 << std::endl;
}


int main(int argc, char *argv[]) {
    int N = std::atoi(argv[1]);
    size_t K = std::strtol(argv[2], nullptr, 10);
    std::string filename(argv[3]);
    omp_set_dynamic(0);
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    int p;
    MPI_Comm_size(MPI_COMM_WORLD, &p);

    int block_lengths[7] = { 1, 1, 1000, 1000, 1000, 1000, 1000};
    MPI_Datatype types[7] = { MPI_UNSIGNED_LONG_LONG, MPI_UNSIGNED_LONG_LONG, MPI_INT, MPI_INT, MPI_INT, MPI_INT, MPI_C_BOOL };
    MPI_Aint offsets[7] = { offsetof(StructSolution , n_vertices),
                            offsetof(StructSolution, n_edges),
                            offsetof(StructSolution, vertices),
                            offsetof(StructSolution, prices),
                            offsetof(StructSolution, edge_start),
                            offsetof(StructSolution, edge_end),
                            offsetof(StructSolution, edge_included)};

    MPI_Type_create_struct(7, block_lengths, offsets, types, &solution_type);
    MPI_Type_commit(&solution_type);
//    MPI_Barrier(MPI_COMM_WORLD); /* IMPORTANT */
//    double start = MPI_Wtime();
    // master
    if (rank == DEST_MASTER) {
        Graph graph = load(N, K, filename);
        run(graph, true);
        std::cout << "Master STOPS" << std::endl;
//        std::ofstream outfile;
//        outfile.open("../out.txt", std::ios_base::app);
//        outfile << "MPI p. @ " << p << " nodes|" N << "C: " << results[i] << std::endl;
    }
    // slave
    else{
        DEPTH_LIMIT = SLAVE_DEPTH_LIMIT;
        omp_set_num_threads(N);
        while (true){
            MPI_Status status;
            StructSolution serialized_solution;
            MPI_Recv(&serialized_solution,
                    1,
                    solution_type, DEST_MASTER, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
            if(status.MPI_TAG == TAG_WORK) {
                Graph graph = Graph(serialized_solution);
                graph.print_state();
                runSlave(graph);
                MPI_Ssend(&best_solution,
                        1,
                        solution_type, DEST_MASTER, TAG_DONE, MPI_COMM_WORLD);
            }
            else if(status.MPI_TAG == TAG_TERMINATE) {
                std::cout << rank << "|thread:" << omp_get_thread_num() << " TERMINATE " <<  std::endl;
                break;
            }
        }
    }
    MPI_Type_free(&solution_type);
    MPI_Barrier(MPI_COMM_WORLD); /* IMPORTANT */

//    double end = MPI_Wtime();
//    if (rank == DEST_MASTER) {
//        double duration = end - start;
//        std::ofstream outfile;
//        outfile.open("../out.txt", std::ios_base::app);
//        outfile << "MPI p. @ " << p << " nodes|" N << "C: " << duration << std::endl;
//    }
    
    MPI_Finalize();
    return 0;
}
