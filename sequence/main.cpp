#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include <algorithm>
#include <chrono>

enum Color{
    EMPTY,
    RED,
    BLUE
};

size_t num_recursions = 0;
size_t best_price = 0;

class Solution{
public:
    Solution()=default;
    Solution(std::vector<Color> & vertices, std::vector<std::pair<int, std::pair<size_t, size_t>>> & used_edges):
            vertices(vertices), used_edges(used_edges){};

    void display_coloring(){
        std::cout << "Red vertices:";
        for (size_t i = 0; i < vertices.size(); ++i){
            Color color = vertices[i];
            if(color == Color::RED){
                std::cout << " " << i;
            }
        }
        std::cout << std::endl << "Blue vertices:";
        for (size_t i = 0; i < vertices.size(); ++i){
            Color color = vertices[i];
            if(color == Color::BLUE){
                std::cout << " " << i;
            }
        }
        std::cout << std::endl << "Edges:" << std::endl <<"Red\t\t Blue";
        for(auto edge:used_edges){
            std::cout << std::endl;
            Color c1 = vertices[edge.second.first];
            if(c1 == Color::RED) {
                std::cout << edge.second.first << "\t<>\t" << edge.second.second;
            }
            else{
                std::cout << edge.second.second << "\t<>\t" << edge.second.first;
            }
        }
        std::cout << std::endl;
    }
private:
    std::vector<Color> vertices;
    std::vector<std::pair<int, std::pair<size_t, size_t>>> used_edges;
};

Solution best_solution;

class Graph{
public:
    Graph(std::vector<Color> & vertices_, std::vector<std::pair<int, std::pair<size_t, size_t>>> & edges_, std::vector<std::pair<int, std::pair<size_t, size_t>>> & used_edges_) {
        vertices = std::vector<Color>(vertices_);
        edges = std::vector<std::pair<int, std::pair<size_t, size_t>>>(edges_);
        used_edges = std::vector<std::pair<int, std::pair<size_t, size_t>>>(used_edges_);
    }

    void explore() {
        if(! is_bipartite()){
            return;
        }
        // BnB with connectivity limit
        // connected graph must have |E| >= |V|-1
        if ((used_edges.size() + edges.size()) < (vertices.size() - 1)){
            return;
        }
        size_t current_price = 0;
        for(auto edge: used_edges){
            current_price += edge.first;
        }
        if (edges.empty()){
            if(current_price > best_price && is_connected(used_edges)){
                best_price = current_price;
                best_solution = Solution(vertices, used_edges);
            }
            return;
        }
        size_t possible_gain = 0;
        for(auto edge: edges){
            possible_gain += edge.first;
        }
        // BnB with upperbound price of the subgraph
        if(current_price + possible_gain <= best_price){
            return;
        }
        if(current_price > best_price && is_connected(used_edges)){
            best_price = current_price;
            best_solution = Solution(vertices, used_edges);
        }
        ++num_recursions;
        auto edge_i = edges.size() - 1;
        size_t v1, v2;
        for(int i = edges.size()-1; i >=0; --i){
            auto e = edges[i];
            v1 = e.second.first, v2 = e.second.second;
            if(vertices[v1] != Color::EMPTY || vertices[v2] != Color::EMPTY){
                edge_i = i;
                break;
            }
        }
        auto top_edge = edges[edge_i];
        v1 = top_edge.second.first, v2 = top_edge.second.second;
        Color c1 = vertices[v1], c2 = vertices[v2];
        edges.erase(edges.begin()+edge_i);
        // edges are sorted in ascending order
        used_edges.push_back(top_edge);

        // the edge can be trivially included because both ends are have color
        // do not explore without it
        if(vertices[v1] != vertices[v2] && vertices[v1] != Color::EMPTY && vertices[v2] != Color::EMPTY){
            Graph graph(vertices, edges, used_edges);
            graph.explore();
            return;
        }
        if(vertices[v1] == Color::EMPTY && vertices[v2] != Color::EMPTY){
            vertices[v1] = vertices[v2] == Color::RED ? Color::BLUE : Color::RED;
            Graph graph(vertices, edges, used_edges);
            graph.explore();
        }
        else if(vertices[v2] == Color::EMPTY && vertices[v1] != Color::EMPTY){
            vertices[v2] = vertices[v1] == Color::RED ? Color::BLUE : Color::RED;
            Graph graph(vertices, edges, used_edges);
            graph.explore();
        }
        else if(vertices[v1] == Color::EMPTY && vertices[v2] == Color::EMPTY){
            // color edge Red
            vertices[v1] = Color::RED;
            // opposite color
            vertices[v2] = Color::BLUE;
            // continue recursion with another edge
            Graph graph(vertices, edges, used_edges);
            graph.explore();
            // color edge Blue
            vertices[v1] = Color::BLUE;
            // opposite color
            vertices[v2] = Color::RED;
            graph = Graph(vertices, edges, used_edges);
            graph.explore();
        }

        // do not include this edge, remove it and continue with recursion
        // this edge was not included so if any vertex is colored the other can be colored the same
        used_edges.pop_back();
        if(c1 == Color::EMPTY) {
            vertices[v1] = c2;
            vertices[v2] = c2;
        }else if(c2 == Color::EMPTY) {
            vertices[v1] = c1;
            vertices[v2] = c1;
        }
        explore();
    }

    bool is_connected(std::vector<std::pair<int, std::pair<size_t, size_t>>> & edges_){
        // test of components, if each vertex has same number there is only one component and graph is connected
        std::vector<std::pair<int, std::pair<size_t, size_t>>> connectivity(edges_);
        auto top_edge = connectivity.front();
        size_t current_v = top_edge.second.first;
        size_t top_v = current_v;
        // propagate number top_v to whole component
        propagate(current_v, top_v, connectivity);
        for(auto edge: connectivity){
            auto v1 = edge.second.first, v2 = edge.second.second;
            if (v1 != top_v || v2 != top_v) return false;
        }
        return true;
    }

    bool propagate(size_t current_v, size_t top_v, std::vector<std::pair<int, std::pair<size_t, size_t>>> & connectivity){
        for(size_t i = 0; i < connectivity.size(); ++i){
            auto v1 = connectivity[i].second.first, v2 = connectivity[i].second.second;
            if(v1 == current_v && v2 != top_v){
                connectivity[i].second.first = top_v;
                connectivity[i].second.second = top_v;
                propagate(v2, top_v, connectivity);
            } else if(v2 == current_v && v1 != top_v){
                connectivity[i].second.first = top_v;
                connectivity[i].second.second = top_v;
                propagate(v1, top_v, connectivity);
            }
        }
    }

    bool is_bipartite() {
        // tests if all connected vertices have different colors
        for(auto e: used_edges) {
            auto v1 = e.second.first;
            auto v2 = e.second.second;
            // conflict of the same color
            if(vertices[v1] == vertices[v2] && vertices[v1] != Color::EMPTY){
                return false;
            }
        }
        return true;
    }

private:
    std::vector<Color> vertices;
    std::vector<std::pair<int, std::pair<size_t, size_t>>> edges;
    std::vector<std::pair<int, std::pair<size_t, size_t>>> used_edges;
};
Graph load(size_t N, size_t K, const std::string& filename){
    std::ifstream infile(filename);
    size_t n;
    // upper bound
    size_t ub = 0;
    infile >> n;
    // price of the edge in 2D adjacency matrix
    std::vector<std::vector<int>> adj_matrix(n, std::vector<int>(n, 0));
    // price, <v1, v2>
    // edges do not repeat
    std::vector<std::pair<int, std::pair<size_t, size_t>>> edges;
    for (size_t i = 0; i < n; ++i) {
        for (size_t j = 0; j < n; ++j) {
            // price is non-negative
            int price;
            infile >> price;
            adj_matrix[i][j] = price;
            // do not repeat the same edge
            if (adj_matrix[i][j] != adj_matrix[j][i]) {
                edges.emplace_back(price, std::pair(i, j));
                ub += price;
            }
        }
    }
    std::vector<Color> vertices(n, Color::EMPTY);
    std::vector<std::pair<int, std::pair<size_t, size_t>>> used_edges;

    // increasing order
    std::sort(edges.begin(), edges.end(), [](auto &left, auto &right)
    {return left.first < right.first;});
    Graph graph(vertices, edges, used_edges);
    if(! graph.is_connected(edges)){
        std::cout << filename << " graph is not connected." << std::endl;
    }
    return graph;
}

void run(Graph & graph, bool display=false){
    auto start = std::chrono::steady_clock::now();
    graph.explore();
    auto stop = std::chrono::steady_clock::now();
    if(display) best_solution.display_coloring();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - start);
    std::cout << "Total price:\t\t" << best_price << std::endl << "# of recursions:\t" << num_recursions / 1000
    << "K" << std::endl << "Time in seconds:\t"<< duration.count() / 1000 << std::endl;
    std::ofstream outfile;
    outfile.open("../out.txt", std::ios_base::app);
    outfile << "Seq: " << double(duration.count()) / 1000; << std::endl; 
}


void testing(std::string filename){
    std::ifstream infile(filename);
    std::string graph_file;
    double ref_time;
    size_t ref_num;
    size_t ref_price;
    size_t passed_ctr = 0;
    size_t total_ctr = 0;
    while (true) {
        infile >> graph_file >> std::skipws >> ref_time >> ref_num >> ref_price;
        if( infile.eof() ) break;
        ++total_ctr;
        best_price = 0;
        num_recursions = 0;
        graph_file.insert(0, "graf_bpo/");
        Graph graph = load(0, 0, graph_file);
        std::cout << graph_file << std::endl;
        run(graph, false);
        std::string result = best_price == ref_price ? "PASSED" : "FAILED";
        if (best_price == ref_price) ++passed_ctr;
        std::cout << "REFERENCE:" << std::endl;
        std::cout << "Total price:\t\t" << ref_price << std::endl << "# of recursions:\t" << ref_num << "K" << std::endl << "Time in seconds:\t"<< ref_time << std::endl;
        std::cout << "====================" << result << "====================" << std::endl;
    }
    std::cout << passed_ctr << "/" << total_ctr << " passed." << std::endl;
}



int main(int argc, char *argv[]) {
    if(argc == 2){
        std::string filename(argv[1]);
        std::cout << "====================" << "TESTING" << "====================" << std::endl;
        testing(filename);
        return 0;
    }
    size_t N = std::strtol(argv[1], nullptr, 10);
    size_t K = std::strtol(argv[2], nullptr, 10);
    std::string filename(argv[3]);
    Graph graph = load(N, K, filename);
    run(graph, true);
}